# SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: AGPL-3.0-or-later

import re
import time
import json
import pycurl
import asyncio
import datetime

# import tornado.httpclient
import tornado.curl_httpclient
from collections import deque
from zoneinfo import ZoneInfo

import fdroidmonitor.main
import fdroidmonitor.matrix_send

UTC = ZoneInfo('UTC')

RUNNING = True

ONION_RE = re.compile(r'.*[a-z0-9]{56}\.onion')

REFRESH = 12 * 60 * 60  # every 12 hours
OWNED_DOMAINS = [
    "f-droid.org",
    "fdroid.org",
    "f-droid.com",
    "fdroid.com",
    "fdroid.link",
]
EXPECTED_TLS_CERTS = {
    "verification.f-droid.org": {"expected_cas": ["Let's Encrypt"]},
    "search.f-droid.org": {"expected_cas": ["Let's Encrypt"]},
    "staging.f-droid.org": {"expected_cas": ["Let's Encrypt"]},
    "cloudflare.f-droid.org": {
        "expected_cas": ["Google Trust Services"],
        "max_certs": 6,
    },
    "monitor.f-droid.org": {"expected_cas": ["Let's Encrypt"]},
    "matrix.f-droid.org": {"expected_cas": ["Let's Encrypt"]},
    "news.f-droid.org": {"expected_cas": ["Let's Encrypt"]},
    "forum.f-droid.org": {"expected_cas": ["Let's Encrypt"]},
    "f-droid.org": {"expected_cas": ["Let's Encrypt"]},
    "www.f-droid.org": {"expected_cas": ["Let's Encrypt"]},
    "fdroid.org": {"expected_cas": ["Let's Encrypt"]},
    "www.fdroid.org": {"expected_cas": ["Let's Encrypt"]},
    "openpgpkey.f-droid.org": {"expected_cas": ["Let's Encrypt"]},
    "fdroid.link": {"expected_cas": ["Let's Encrypt"]},
    "fdroid.com": {"expected_cas": ["Let's Encrypt"]},
    "f-droid.com": {"expected_cas": ["Let's Encrypt"]},
    "www.fdroid.com": {"expected_cas": ["Let's Encrypt"]},
    "www.f-droid.com": {"expected_cas": ["Let's Encrypt"]},
}
TLS_CERTS = {}
DEFAULT_MAX_CERTS = 2


async def services_loop():
    last_run = 0

    # fill optioninal values with defaults
    for cdef in EXPECTED_TLS_CERTS.values():
        if "max_certs" not in cdef:
            cdef["max_certs"] = DEFAULT_MAX_CERTS

    # start main loop of this background service
    while RUNNING:
        now = time.time()
        if last_run + REFRESH < now:
            last_run = now
            tornado.ioloop.IOLoop.current().spawn_callback(update_tls_tranparency_logs)
        await asyncio.sleep(1)


async def tls_cert_check_domain(domain):
    url = f'https://api.certspotter.com/v1/issuances?domain={domain}&expand=issuer&include_subdomains=true&expand=dns_names'
    http_client = tornado.httpclient.AsyncHTTPClient()
    try:
        response = await http_client.fetch(url)
        if response.error:
            raise response.error
    except tornado.httpclient.HTTPClientError as e:
        raise fdroidmonitor.main.MyException(
            "HTTP 502: Bad Gateway",
            f"Cert Spotter API sent and unexpected response - {e}",
            502,
        ) from e
    else:
        return json.loads(response.body)


async def fetch_tls_tansparency_logs():
    certs = {}
    for domain in OWNED_DOMAINS:
        cert_datas = await tls_cert_check_domain(domain)
        for cert_data in cert_datas:
            for dns_name in cert_data['dns_names']:
                if dns_name not in certs:
                    certs[dns_name] = {"certs": []}
                # store cert data from the the API, eliminate duplicated though
                if not any(
                    c['id'] == cert_data['id'] for c in certs[dns_name]['certs']
                ):
                    certs[dns_name]['certs'].append(cert_data)
    return certs


async def fetch_and_assert_tls_transparency_logs():
    certs = await fetch_tls_tansparency_logs()
    for certs_info in certs.values():
        certs_info['asserts'] = {
            "unknown_subdomains": [],
            'too_many_certs': False,
            'unexpected_cas': [],
        }

    # look for unknown sub-domains
    for sub_domain, certs_info in certs.items():
        for cert in certs_info['certs']:
            for dns_name in cert['dns_names']:
                if dns_name not in EXPECTED_TLS_CERTS:
                    if dns_name not in certs_info['asserts']['unknown_subdomains']:
                        certs_info['asserts']['unknown_subdomains'].append(dns_name)

    # check number of issued certificates
    for sub_domain, certs_info in certs.items():
        if len(certs_info['certs']) > EXPECTED_TLS_CERTS.get(sub_domain, {}).get(
            "max_certs", DEFAULT_MAX_CERTS
        ):
            certs_info['asserts']['too_many_certs'] = certs_info['certs']

    # check for certificates issued by CAs that are not on our list of expected CAs
    for sub_domain, certs_info in certs.items():
        for cert in certs_info['certs']:
            expected_cas = EXPECTED_TLS_CERTS.get(sub_domain, {}).get(
                'expected_cas', []
            )
            found_ca = cert['issuer']['friendly_name']
            if found_ca not in expected_cas:
                if found_ca not in certs_info['asserts']['unexpected_cas']:
                    certs_info['asserts']['unexpected_cas'].append(found_ca)

    return certs


async def update_tls_tranparency_logs():
    c = await fetch_and_assert_tls_transparency_logs()
    # just update the content not the instance itself, so other modueles are
    # accessing the same dict
    TLS_CERTS.clear()
    TLS_CERTS.update(c)

    for domain, cert in c.items():
        if cert['asserts']['unknown_subdomains']:
            for s in cert['asserts']['unknown_subdomains']:
                c_ = ', '.join([c['cert_sha256'] for c in cert['certs']])
                await fdroidmonitor.matrix_send.matrix_send_devops(
                    f"⚠️  found an unknown subdomain '{s}' in certificate: {c_} "
                    f"(https://monitor.f-droid.org/services/tls-certs#{s})"
                )
        if cert['asserts']['too_many_certs']:
            c_ = ', '.join([c['cert_sha256'] for c in cert['certs']])
            await fdroidmonitor.matrix_send.matrix_send_devops(
                f"⚠️  expected {EXPECTED_TLS_CERTS[domain]['max_certs']} for '{domain}' but found {cert['asserts']['too_many_certs']} active certificates: {c_} "
                f"(https://monitor.f-droid.org/services/tls-certs)"
            )
