# SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: AGPL-3.0-or-later

import re
import time
import pycurl
import asyncio
import datetime

# import tornado.httpclient
import tornado.curl_httpclient
from collections import deque
from zoneinfo import ZoneInfo

UTC = ZoneInfo('UTC')

RUNNING = True

ONION_RE = re.compile(r'.*[a-z0-9]{56}\.onion')

DEFAULT_REFRESH = 5 * 60
SERVICE_TIMESERIES_LEN = 50
SERVICE_DEFS = {
    "Website": {
        "url": "https://f-droid.org",
        "section": "web",
    },
    "Website (Tor)": {
        "url": "http://fdroidorg6cooksyluodepej4erfctzk7rrjpjbbr6wx24jh3lqyfwyd.onion/",
        "socks5": "127.0.0.1:9050",
        "section": "web",
    },
    "Website (Cloudflare)": {
        "url": "https://cloudflare.f-droid.org/",
        "section": "web",
    },
    "Download": {
        "url": "https://f-droid.org/F-Droid.apk",
        "section": "web",
    },
    "Search": {
        "url": "https://search.f-droid.org/",
        "section": "web",
    },
    "Link": {
        "url": "https://fdroid.link/",
        "section": "web",
    },
    "Verification": {
        "url": "https://verification.f-droid.org/",
        "section": "web",
    },
    "Mastodon": {
        "url": "https://floss.social/@fdroidorg",
        "section": "web",
    },
    "Forum": {
        "url": "https://forum.f-droid.org",
        "section": "web",
    },
    "f-droid.org": {
        "url": "https://f-droid.org/repo/entry.jar",
        "section": "mirrors",
        "link": False,
    },
    "fdroidorg6cooksyluodepej4erfctzk7rrjpjbbr6wx24jh3lqyfwyd.onion": {
        "url": "http://fdroidorg6cooksyluodepej4erfctzk7rrjpjbbr6wx24jh3lqyfwyd.onion/fdroid/repo/entry.jar",
        "section": "mirrors",
        "link": False,
    },
    "fdroid.tetaneutral.net": {
        "url": "https://fdroid.tetaneutral.net/fdroid/repo/entry.jar",
        "section": "mirrors",
        "link": False,
    },
    "ftp.agdsn.de": {
        "url": "https://ftp.agdsn.de/fdroid/repo/entry.jar",
        "section": "mirrors",
        "link": False,
    },
    "ftp.fau.de": {
        "url": "https://ftp.fau.de/fdroid/repo/entry.jar",
        "section": "mirrors",
        "link": False,
    },
    "ftp.lysator.liu.se": {
        "url": "https://ftp.lysator.liu.se/fdroid/repo/entry.jar",
        "section": "mirrors",
        "link": False,
    },
    "ftpfaudev4triw2vxiwzf4334e3mynz7osqgtozhbc77fixncqzbyoyd.onion": {
        "url": "http://ftpfaudev4triw2vxiwzf4334e3mynz7osqgtozhbc77fixncqzbyoyd.onion/fdroid/repo/entry.jar",
        "section": "mirrors",
        "link": False,
    },
    "lysator7eknrfl47rlyxvgeamrv7ucefgrrlhk7rouv3sna25asetwid.onion": {
        "url": "http://lysator7eknrfl47rlyxvgeamrv7ucefgrrlhk7rouv3sna25asetwid.onion/fdroid/repo/entry.jar",
        "section": "mirrors",
        "link": False,
    },
    "mirror.cyberbits.eu": {
        "url": "https://mirror.cyberbits.eu/fdroid/repo/entry.jar",
        "section": "mirrors",
        "link": False,
    },
    "mirror.fcix.net": {
        "url": "https://mirror.fcix.net/fdroid/repo/entry.jar",
        "section": "mirrors",
        "link": False,
    },
    "mirror.ossplanet.net": {
        "url": "https://mirror.ossplanet.net/fdroid/repo/entry.jar",
        "section": "mirrors",
        "link": False,
    },
    "plug-mirror.rcac.purdue.edu": {
        "url": "https://plug-mirror.rcac.purdue.edu/fdroid/repo/entry.jar",
        "section": "mirrors",
        "link": False,
    },
    "http02 (Finland)": {
        "url": "http://http02.fdroid.net",
        "section": "proxies",
        "link": False,
    },
    "http03 (Netherlands)": {
        "url": "http://http03.fdroid.net",
        "section": "proxies",
        "link": False,
    },
}
SERVICE_TIMESERIES = {}
SECTION_DEFS = {
    "web": {
        "title": "Web",
        "description": "",
    },
    "mirrors": {
        "title": "Mirrors",
        "description": "",
    },
    "proxies": {
        "title": "reverse proxies",
        "description": "",
    },
}


def prepare_curl_socks5(curl):
    curl.setopt(pycurl.PROXYTYPE, pycurl.PROXYTYPE_SOCKS5_HOSTNAME)


async def services_loop():
    for name, defs in SERVICE_DEFS.items():
        SERVICE_TIMESERIES[name] = deque([], maxlen=SERVICE_TIMESERIES_LEN)
        if "link" not in defs:
            defs['link'] = True
        if "last_run" not in defs:
            defs['last_run'] = 0
        if "refresh_interval" not in defs:
            defs["refresh_interval"] = DEFAULT_REFRESH

    while RUNNING:
        for name, defs in SERVICE_DEFS.items():
            now = time.time()
            if defs["last_run"] + defs["refresh_interval"] < now:
                defs["last_run"] = now
                # await update_service_timeseries(name, defs)
                tornado.ioloop.IOLoop.current().spawn_callback(
                    update_service_timeseries, name, defs
                )
        await asyncio.sleep(1)


async def update_service_timeseries(name, defs):
    err = []
    tornado.httpclient.AsyncHTTPClient.configure(
        "tornado.curl_httpclient.CurlAsyncHTTPClient"
    )
    http_client = tornado.httpclient.AsyncHTTPClient()
    retries = defs.get("retries", 3)
    is_onion = bool(ONION_RE.match(defs['url']))
    for attempt in range(retries):
        try:
            resp = await http_client.fetch(
                request=tornado.curl_httpclient.HTTPRequest(
                    defs['url'],
                    prepare_curl_callback=prepare_curl_socks5,
                    proxy_host="localhost",
                    proxy_port=9050,
                    method="HEAD",
                )
            )
            if resp.code == 200:
                break
        except Exception as e:
            err.append(f"{e} ({type(e)})")
    # Instead of returing any values, we just write them to our timeseries.
    # This way it'll stay in RAM, an the http handler can access it at any time.
    date = datetime.datetime.utcnow()  # .replace(tzinfo=UTC)
    SERVICE_TIMESERIES[name].appendleft(
        {
            'online': "y" if len(err) == 0 else "p" if len(err) != retries else "n",
            'timestamp': date,
            'message': "errors:\n{}\n\n".format("\n".join(err)) if err else "",
        }
    )
