# SPDX-FileCopyrightText: 2024 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: AGPL-3.0-or-later

import json
import tornado.httpclient
import fdroidmonitor.cfg

http_client = None


async def matrix_send_devops(msg):
    cfg = fdroidmonitor.cfg.load()
    if (
        not cfg.get("matrix_bot_server")
        or not cfg.get("matrix_bot_token")
        or not cfg.get("matrix_bot_devops_room")
    ):
        # can't send, because it's not configured, skip
        return

    url = f"{cfg['matrix_bot_server']}_matrix/client/r0/rooms/{cfg['matrix_bot_devops_room']}/send/m.room.message"
    data = {
        "msgtype": "m.text",
        "body": msg,
    }
    headers = {
        "Authorization": f"Bearer {cfg['matrix_bot_token']}",
        "Content-Type": "application/json",
    }

    if fdroidmonitor.matrix_send.http_client is None:
        fdroidmonitor.matrix_send.http_client = tornado.httpclient.AsyncHTTPClient()
    request = tornado.httpclient.HTTPRequest(
        url,
        method='POST',
        body=json.dumps(data),
        headers=headers,
    )
    response = await fdroidmonitor.matrix_send.http_client.fetch(request)
    return json.loads(response.body)
